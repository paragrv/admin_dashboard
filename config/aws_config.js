module.exports = {

    development: { // CREDS - DEV

        accessKeyId: 'AKIA2IYDDSCDU4RAWVSY',
        secretAccessKey: '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',
        region: 'ap-south-1',
        bucket: 'dev-skandha-vod',

        transcode_module: 'transcode',
        adv_transcode_module: 'adv-transcode',
        data_science_module: 'data-science',

        putfolder: 'input-data',
        getfolder: 'processed-data',
        getfolder_assets: 'assets',
        thumbnail_folder: 'thumbnails',

        SNS_CONFIG: {
            TopicArn: {
                preview: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-preview-sns',
                basic: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-basic-sns',
                standard: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-standard-sns',
                premium: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-premium-sns',
            }
        },

        MEDIACONVERT_CONFIG: {
            accessKeyId: 'AKIA2IYDDSCDU4RAWVSY',
            secretAccessKey: '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',
            endpoint: 'https://idej2gpma.mediaconvert.ap-south-1.amazonaws.com',
            region: 'ap-south-1',
        },

        DISNEY_CONFIG: {

            bucket: 'transcoding-rawmedia',
            previewFolder: 'disney-low-resolution',
            delimiter: '/',
            dynamoDB_table: 'qa-dam-extract-lang',
            // TopicArn: 'arn:aws:sns:ap-south-1:705991774343:qa_disney_multi_lang_sns'
            TopicArn: 'arn:aws:sns:ap-south-1:705991774343:qa_multilang_dynamic_disney_sns_topic'
        },

        AZURE: {
            dynamoDB_table: 'qa-dam-content-metadata-db',
            TopicArn: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-content-metadata-sns'
        },

        COGNITO: {
            // UserPoolId: 'ap-south-1_Cc5UVmTye',
            // ClientId: '6q9odla4105a66klgbqaqbh9tu'
            UserPoolId: 'ap-south-1_9qwaFqeqg',
            ClientId: '5sk14ic79hhd3qqt89kv3b3glp'
          
        },

        video: {
            extension: '.mp4',
            index: 'index.m3u8',
        },

        DYNAMO_DB: {
            tables: {
                previewTranscode: 'qa-dam-preview',
                basic_transcode: 'qa-dam-basic-db',
                standard_transcode: 'qa-dam-standard-db',
                premium_transcode: 'qa-dam-premium-db',
                custom_transcode: 'qa-dam-custom-db'
            }
        },

        image: {
            extension: '.jpg',
        },

        IMDB: {
            apiKey: 'dbd0550e'
        },

        cloudformation: {
            lambdaFunction: 'qa-cloudformation-basic-model-lambda',
            configDynamodbTable: 'qa-dam-tenantInfo'
        }
    },



    production: { // CREDS - PROD
    },


};
