module.exports = {

    // ############### MICRO-1 ###############
    'mcube_transcode': {
        "base": {
           'development': 'http://35.154.153.103:3001',
           //'development': 'http://localhost:3001',
            'staging': '',
            'production': ''
        },

        'video': {
            'list': '/api/video/list',
            'todays_uploaded': '/api/video/todays_uploaded',
            'transcode_asset': '/api/video/transcode_asset',
            'transcode_custom_asset': '/api/video/transcode_custom_asset',
            'deletevideo': '/api/video/deletevideo/',
            'metadatasave': '/api/video/metadatasave',
            'preload_metadata': '/api/video/preload_metadata',
            'get_asset_status': '/api/video/get_asset_status'
        },

        'dashboard': {
            'proccessingList': '/api/dashboard/proccessing_list',
            'todays_uploaded': '/api/dashboard/todays_uploaded',
            'proccessing_list': '/api/dashboard/proccessing_list',
            'transcoded_list': '/api/dashboard/transcoded_list'
        },
        'profile':{
                    'getAllProfiles':'/api/profile/getAllProfiles',
                    'deleteProfile':'/api/profile/deleteProfile/',
                    'preload_profiledata':'/api/profile/preload_profiledata'

        },
        'settings':{
                      'updateConfig':'/api/settings/updateConfig',

         },

         'user': {
            'login': '/login',
            'passwordUpdate': '/password/update'
        }
    },

    // ############### MICRO-2 ###############
    'mcube_adv_transcode': {
        "base": {
           'development': 'http://35.154.153.103:3002',
           // 'development': 'http://localhost:3002',
            'staging': '',
            'production': ''
        },

        'video': {
            'listS3_Assets': '/api/video/listS3_Assets',
            'createPreview': '/api/video/create-preview',
            'getlinkPreview': '/api/video/getlink-preview',
            'listS3_PreviewAssets': '/api/video/listS3_PreviewAssets',
            'listS3ContentMetadatatDynamodb': '/api/video/listS3ContentMetadatatDynamodb',
            'transcodeAssetDisney': '/api/video/transcode-asset-disney',
            'createMetaDataAzure': '/api/video/create-meta-data-azure',
            'getMetaDataAzure': '/api/video/get-meta-data-azure',
            'getImdbData': '/api/video/getImdbData',
            'updateImdbStatus': '/api/video/updateImdbStatus',
            'findMetaDataImdb': '/api/video/findMetaData-imdb/', // sending in params (not body)
            'getMediaConvertStatus': '/api/video/getMediaConvertStatus/'
        }
    },

    // ############### MICRO-3 ###############
    'mcube_cognito': {
        "base": {
            'development': 'http://35.154.153.103:3003',
            //'development': 'http://localhost:3003',
            'staging': '',
            'production': ''
        },

        'users': {
            'confirmSignUp': '/confirmSignUp',
            'login': '/index/login',
            'authenticateUser': '/index/authenticateUser',
            'register': '/api/users/register',
            'createGroup': '/api/users/createGroup',
            'getUsers': '/api/users/getUsers',
            'deleteUser': '/api/users/deleteUser',
            'getUserInfo': '/api/users/getUserInfo/',  // sending in params (not body)
            'disableUser': '/api/users/disableUser',
            'enableUser': '/api/users/enableUser',
            'changePassword': '/api/users/changePassword',
            'addUserToGroup': '/api/users/addUserToGroup',
            'getListOfGroups': '/api/users/getListOfGroups',
            'deleteGroup': '/api/users/deleteGroup',
            'getGroupInfo': '/api/users/getGroupInfo/',    // sending in params(not body)
            'listUsersInGroup': '/api/users/listUsersInGroup/',
            'createGroup': '/api/users/createGroup',
            'adminRemoveUserFromGroup': '/api/users/adminRemoveUserFromGroup',
            'updateUserAttributes': '/api/users/updateUserAttributes',

        }
    }

}


