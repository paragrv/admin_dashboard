const tenant = require('../app/models/tenant');


class TenantCls {

    TenantCls() { }

    save(params) {
        return new Promise((resolve, reject) => {
            const TenantDocument = new tenant({
                clientId: params.clientId ? params.clientId : null,
                tenantName: params.tenantName ? params.tenantName : null,
                tenantId: params.tenantId ? params.tenantId : null,
                created_by: 'Admin',
            });
            console.log('SAVE' + JSON.stringify(TenantDocument));
            TenantDocument.save((err, tenantData) => {
                if (err) {
                    console.log(`err in saving video: ${err}`);
                    reject(err);
                } else {
                    console.log("SAVED VIDEO DATA" + JSON.stringify(tenantData));
                    resolve(tenantData);
                }
            });
        });
    }

    // Delete Tenant
    deleteTenant(params) {
        return new Promise((resolve, reject) => {
            const obj = {};
            tenant.deleteOne({ tenantId: params.userPoolId })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Tenant SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }
    findBytenantId(params) {
        return new Promise((resolve, reject) => {
            tenant.find({ tenantName: params.tenantName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        console.log("data > "+JSON.stringify(data))
                        resolve(data);
                    }
                });
        })
    }


}


module.exports = {
    TenantClass: TenantCls,
};