const userActivity = require('../app/models/userActivity');


class userActivityCls {
userActivityCls() { }


    // Find Channel By userName 
    findUseractivityByUserName(params) {
        return new Promise((resolve, reject) => {
            userActivity.find({ userName: params.userName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

}


module.exports = {
    userActivityClass: userActivityCls,
};