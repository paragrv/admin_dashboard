const awsInfo = require('../app/models/awsInfo');


class AwsInfoCls {

    AwsInfoCls() { }

    save(params) {
        return new Promise((resolve, reject) => {
            const awsInfoDocument = new awsInfo({
                accessKey: params.accessKey ? params.accessKey : null,
                secretAccessKey: params.secretAccessKey ? params.secretAccessKey : null,
                region: params.region ? params.region : null,
                tenantName_awsInfo: params.tenantName_awsInfo ? params.tenantName_awsInfo : null,
                created_by: 'Admin',
            });
            console.log('SAVE' + JSON.stringify(awsInfoDocument));
            awsInfoDocument.save((err, awsData) => {
                if (err) {
                    console.log(`err in saving video: ${err}`);
                    reject(err);
                } else {
                    console.log("SAVED VIDEO DATA" + JSON.stringify(awsData));
                    resolve(awsData);
                }
            });
        });
    }


     // Find Data By Termination Policy
     findBytenantName_awsInfo(params) {
        return new Promise((resolve, reject) => {
            awsInfo.find({ tenantName_awsInfo: params.tenantName_awsInfo })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }
    
}


module.exports = {
    AwsInfoClass: AwsInfoCls,
};