var AWS = require('aws-sdk');
const util = require('util');
// eslint-disable-next-line no-undef

const awsConfig = require('../../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

const s3 = new AWS.S3();

//////// /////// ################ %% SNS TRIGGERERING %% ################    ///////////

// To insert into DynamoDB
exports.updateDynamodb = (reqParams) => {
    return new Promise((resolve, reject) => {

        var docClient = new AWS.DynamoDB.DocumentClient()

        var params = {
            TableName: reqParams.TableName,
            Key: {
                'assetid': reqParams.assetid
            },
            UpdateExpression: "set audio_info = :a",
            ExpressionAttributeValues: {
                ":a": reqParams.audio_info
            },
            ReturnValues: "UPDATED_NEW"
        }

        docClient.update(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            }
            else { 
                // console.log("updateDynamodbPromise succeeded for Update Audio status:." + JSON.stringify(data));
                resolve()
            }
        });

    })
};
