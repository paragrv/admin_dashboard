const AWS = require('aws-sdk');
// eslint-disable-next-line no-undef
const awsConfig = require('../../config/aws_config')[ENV];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
const s3 = new AWS.S3();

// exports.s3Copy = function(reqParams) {
exports.s3Copy = reqParams => new Promise((resolve, reject) => {
    console.log(`s3Copy reqParams>>>> ${JSON.stringify(reqParams)}`);
    s3.copyObject({
        Bucket: reqParams.Bucket,
        CopySource: reqParams.Copy_Source,
        Key: reqParams.NEW_KEY,
        ACL: 'public-read',
    })
    .promise()
    .then(() => {
        console.log(`COPIED S3 FILE : ${reqParams.NEW_KEY}`);
        resolve();
    }).catch((ex) => {
        console.log(`ERR in S3 COPY: ${ex}`);
        reject(ex);
    });
});
