const AWS = require('aws-sdk');
const util = require('util');
// eslint-disable-next-line no-undef
const awsConfig = require('../../config/aws_config')[ENV];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

const s3 = new AWS.S3();

exports.processinglistS3Assets = reqParams => new Promise((resolve, reject) => {

    var params = {
        Bucket : reqParams.bucket
    };
    s3.processinglistObjects(params, function (err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else {

           // console.log("listS3Assets Data > " + JSON.stringify(data));


            // var movFiles = data.Contents.filter(
            //     function (file) {
            //         return (file.Key.indexOf('.mov') !== -1);
            //     }
            // );
            // resolve(movFiles)
            resolve(data)


        }
    });

})