const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');
const awsConfig = require('../../../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

exports.createUserPoolApp = (paramsReq) => {
    return new Promise((resolve, reject) => {

      // var policies1 = JSON.parse(JSON.stringify(paramsReq.policies.PasswordPolicy))
        //console.log(policies1)

        var params = {
            PoolName: paramsReq.poolName,
            UserPoolAddOns: paramsReq.userPoolAddOns,
            Policies:policies1,
            MfaConfiguration: paramsReq.mfaConfiguration
        }
        console.log("user pool create params > " + JSON.stringify(params))
        cognitoidentityserviceprovider.createUserPool(params, function (err, data) {
            if (err) {
                reject(err)
            } else {
                console.log('Err in create pool data is ' + JSON.stringify(data));
                resolve(data)
            }
        });
    })
};

// list user pool
exports.listUserPools = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var params = {
            MaxResults: paramsReq.maxResults,
        }
        console.log("list user pool params > " + JSON.stringify(params))
        cognitoidentityserviceprovider.listUserPools(params, function (err, data) {
            if (err) {
                reject(err)
            } else {
                console.log('list user pool data is ' + JSON.stringify(data));
                resolve(data)
            }
        });
    })
};


// Describe user pool data


exports.describeUserPool = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var params = {
            UserPoolId: paramsReq.userPoolId,
        }
        console.log("describe user pool params > " + JSON.stringify(params))
        cognitoidentityserviceprovider.describeUserPool(params, function (err, data) {
            if (err) {
                reject(err)
            } else {
                console.log('describe user pool data is ' + JSON.stringify(data));
                resolve(data)
            }
        });
    })
};