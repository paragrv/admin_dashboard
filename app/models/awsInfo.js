const mongoose = require('mongoose');

const awsInfoSchema = mongoose.Schema({

    accessKey: { type: String},
    secretAccessKey: { type: String},
    region :{ type: String},
    tenantName_awsInfo:{type: String},
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

awsInfoSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('awsInfo', awsInfoSchema, 'awsInfo');