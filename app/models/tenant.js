const mongoose = require('mongoose');

const TenantSchema = mongoose.Schema({

    clientId: { type: String},
    tenantName: { type: String},
    tenantId :{ type: String},
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

TenantSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('tenant', TenantSchema, 'tenant');