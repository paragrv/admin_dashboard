function saveNotification(params) {

    var params = {
        name: params.name,
        description: params.description,
        //action:params.action
    }
    console.log("notification params data > " + JSON.stringify(params))

    $.post('/notification',
        params,
        function (data, status) {
            if (data.status === 200) {
                getNotification()
                console.log(data.message);
            } else {
                console.log(data.message);
            }
        })
}


// remove notification
$(document).on('click', '.remove_notification', function () {

    $("#notifications_container").empty();
    $(".notification-bell-container").html('<i class="fe-bell noti-icon"></i>')

    $.post('/removeNotification',
        function (data, status) {
            if (data.status === 200) {
                toastr.success(data.message);
            } else {
                toastr.error(data.message);
            }
        })
})


// get notification
function getNotification() {

    $.get('/notification',

        function (data, status) {

            var array = data.data;
            var newNotification = "";
            array.forEach(function (element, i) {

                var name = element.name ? element.name : "";
                var description = element.description ? element.description : "";
                //var action = element.action ? element.action : "";
                var created_at = element.created_at ? moment(element.created_at).format('lll') : "";
                newNotification += `<a href="#" class="dropdown-item notify-item">
            <div class="notify-icon bg-warning">
                <i class="mdi mdi-settings"></i>
            </div>
            <p class="notify-details">
               ${name} 
                <small class="text-muted">${description} at ${created_at}</small>
            </p>
          </a>`
            })

            $('#notifications_container').html(newNotification)
            var notificationCount = $('.notify-icon').length;
            if (notificationCount > 0) {
                $(".notification-bell-container").html(`<i class="fe-bell noti-icon"></i><span id="notify-count" class="badge badge-danger rounded-circle noti-icon-badge">${notificationCount}</span>`)
            }

        })
}

getNotification()