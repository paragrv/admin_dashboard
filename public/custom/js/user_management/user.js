
$(document).ready(function () {

    // getUsers
    $.get('/v1/users/getUsers', function (data, status) {
        if (data.status == 200) {
            destroyRows();
            appendUsersDatatable(data);
        }
    });



    // getGroup
    $.get('/v1/users/getListOfGroups',
        function (data, status) {
            if (data.status == 200) {
                //console.log("getListOfGroups >> : " + JSON.stringify(data))
                destroyRowsofGroupTable();
                appendGroupsDatatable(data)
            }
        });

    // Login
    $("#login-form").submit(function (event) {
        $.post('/login',
            {
                tenantId: $("#tenantId").val().trim(),
                username: $("#email").val().trim().toLowerCase(),
                password: $("#password").val().trim()
            },
            function (data, status) {
                if (data.status === 200) {
                    window.location.href = '/v1/users';
                } else {
                    toastr.error(data.message);
                }
            });


        event.preventDefault()
    });




    // authenticateUser
    $("#authentication-form").submit(function (event) {
 
        $.post('/authenticateUser',
      
            {
                tenantName: $("#tenantId").val().trim(),
                username: $("#email").val().trim().toLowerCase(),
                password: $("#password").val().trim(),
                newPassword: $("#newPassword").val().trim()
            },
            function (data, status) {
                if (data.status === 200) {
                    toastr.success('Password reset successfully. Sign in to continue.');
                    setTimeout(function () {
                        window.location.href = '/';
                    }, 3000)
                } else {
                    toastr.error(data.message);
                }
            });
        event.preventDefault()
    });


    // Register
    $("#register-form").submit(function (event) {
        $.post('/v1/users/register',
            {
                name: $('#fullName').val().trim(),
                email: $('#email').val().trim().toLowerCase(),
                password: $("#password").val().trim()
            }
            
            ,
            function (data, status) {
                if (data.status === 200) {
                    user = data.data.user;
                    toastr.success('User successfully registered.');
                    $('#userRegisterModal').modal('hide');

                    setTimeout(function () {
                        window.location.href = '/v1/users'
                        // $('#confirm-modal-email-field').html(user.username)
                        // $('#verifyCodeModal').modal('show');
                    }, 3000)

                } else {
                  //  toastr.danger('User registration failed.')
                }

            });
        event.preventDefault()
    })


    // Verify Code
    $("#verify-code-form").submit(function (event) {
        $.post('/v1/users/confirmSignUp',
            {
                ConfirmationCode: $("#verification_code").val().trim(),
                Username: $("#confirm-modal-email-field").text()
            },
            function (data, status) {
                if (data.status === 200) {

                    toastr.success('Successfully verified');
                    setTimeout(function () {
                        window.location.href = '/v1/users';
                    }, 3000)

                } else {
                    toastr.error('User Verification Failed.');
                }
            });
        event.preventDefault()
    })

});

function destroyRows() {
    $('#list_all_users_tbody').empty()
    $('#list_all_user_table').DataTable().rows().remove();
    $("#list_all_user_table").DataTable().destroy()
}


function destroyRowsofGroupTable() {
    $('#list_all_group_tbody').empty()
    $('#list_all_group_table').DataTable().rows().remove();
    $("#list_all_group_table").DataTable().destroy()
}


function appendUsersDatatable(data) {
    var array = data.data.Users;
    if (array.length) {
        var options_table = "";
        array.forEach(function (element, i) {

            var Username = element.Username ? element.Username : "";
            var Attributes = element.Attributes ? element.Attributes : "";
            var UserCreateDate = element.UserCreateDate ? moment(element.UserCreateDate).format('lll') : "";
            var UserLastModifiedDate = element.UserLastModifiedDate ? moment(element.UserLastModifiedDate).format('lll') : "";
            var Enabled = element.Enabled ? element.Enabled : false;
            var UserStatus = element.UserStatus ? element.UserStatus : "UNCONFIRMED";
            var name = '';

            if (UserStatus === 'CONFIRMED') {
                UserStatus = '<span class="badge badge-success">Active</span>'
            } else {
                UserStatus = '<span class="badge badge-purple">Pending Activation</span>'
            }

            if (Attributes.length) {
                for (var j = 0; j < Attributes.length; j++) {
                    if (Attributes[j].Name === 'name') {
                        name = Attributes[j].Value;
                    }
                }
            }

            options_table += `<tr class="users-tbl-row asset-row" id="${Username}">
            <td class="hidegroup"><input id="groupaddname" type="checkbox"></td>
                <td class="username">${Username}</td>
                <td class="name">${name}</td>
                <td class="state">${UserStatus}</td>
                <td class="action-td" id=${Username}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item delete-user">Delete</a> 
                <a href="#"  class="dropdown-item group_Name" data-toggle="modal" data-target="#GroupModal">Group_Name</a>
                </div> </div></td>`;

            if (i == array.length - 1) {

                //initiate for 1st row
                getUserInfo(array[0].Username)
                $('#list_all_users_tbody').append(options_table)
                reInitializeDataTable()
            }
        })
    } else {
        $('#list_all_users_tbody').append(`<div><h6 class="text-primary">No users found.</h6></div>`);
        $('.user-overview-container').html(`<div class="row"> <div class="col-md-6"> </div> <div class="col-md-6 </div> </div><hr>`)
    }

}



  



function reInitializeDataTable() {
    $("#list_all_user_table").DataTable().destroy()
    list_all_user_table = $('#list_all_user_table').DataTable({
        //"order": [[1, "desc"]], // for descending order
        "columnDefs": [
            // { "width": "35%", "targets": 0 },
            { "width": "30%", "targets": 1 }
        ]
    })

    $("#list_all_user_table tbody tr:first").addClass("active");
}



function appendGroupsDatatable(data) {

    var array = data.data.Groups;

    //console.log(JSON.stringify(array))
    if (array.length) {
        var options_table = "";
        array.forEach(function (element, i) {

            var GroupName = element.GroupName ? element.GroupName : "";
            var CreationDate = element.CreationDate ? moment(element.CreationDate).format('lll') : "";
            var LastModifiedDate = element.LastModifiedDate ? moment(element.LastModifiedDate).format('lll') : "";
            var UserPoolId = element.UserPoolId ? element.UserPoolId : "";

            options_table += `<tr class="all-groups-tbl-row asset-row" id="${GroupName}">
                <td class="group-name"><a class="group-name-link" href="/v1/users/user-groups?group-name=${GroupName}">${GroupName}</a></td>
                <td class="creationDate">${CreationDate}</td>
                <td class="lastModifiedDate">${LastModifiedDate}</td>
                <td class="action-td" id=${GroupName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item delete-group">Delete</a> </div> </div></td>`;


            if (i == array.length - 1) {

                //initiate for 1st row
                getGroupInfo(array[0].GroupName)
                $('#list_all_groups_tbody').empty()
                $('#list_all_groups_tbody').append(options_table)

                reInitializegrouptable()
            }
        })
    } else {
        $('#list_all_groups_tbody').append(`<div class=""><h6>Group not found</h6></div>`);

    }

}



function reInitializegrouptable() {
    $("#list_all_groups_table").DataTable().destroy()
    list_all_group_table = $('#list_all_groups_table').DataTable({
        "order": [[1, "desc"]], // for descending order
        "columnDefs": [
            { "width": "35%", "targets": 0 },
            { "width": "30%", "targets": 1 }
        ]
    })

    $("#list_all_groups_tbody tr:first").addClass("active");
}


function formatGroupInfo(data) {

    var GroupName = data.GroupName
    var CreationDate = data.CreationDate
    var LastModifiedDate = data.LastModifiedDate
    var UserPoolId = data.UserPoolId
    var RoleArn = data.RoleArn

    //console.log(JSON.stringify(GroupName))
    options_groupinfo = `<div class="row" id="group-overview-container">
    <div class="col-md-12"><h4 class="info-groupname">${GroupName}</h4></div>
    <div class="col-md-12">
    <div class="row" id="group-info-container">
    <div class="col-md-5"><h5 class="info-user-pool-id text-muted">User Pool Id : </h5></div><div class="col-md-7" style="padding-top: 8px;">${UserPoolId}</div>
    <div class="col-md-5"><h5 class="info-creation-date text-muted">Creation Date : </h5></div><div class="col-md-7" style="padding-top: 8px;">${moment(CreationDate).format('lll')}</div>
    <div class="col-md-5"><h5 class="info-modified-date text-muted">Last Modified Date : </h5></div><div class="col-md-7" style="padding-top: 8px;">${moment(LastModifiedDate).format('lll')}</div>
    </div>
    </div>
    </div>`

    $('.group-overview-container').html(options_groupinfo)

}


// Delete User
$(document).on("click", ".delete-user", function () {

    var params = {
        username: $(this).closest(".action-td").attr('id'),
    }

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if (t.value) {
            $.post('/v1/users/deleteUser',
                params,
                function (data, status) {
                    if (data.status === 200) {

                        toastr.success('User Deleted');

                        setTimeout(function () {
                            window.location.href = '/v1/users';
                        }, 3000)

                    } else if (data.status === 401) {
                        toastr.error('Delete User Failed');
                    }
                });
        }

    })

})


// formatUserInfo
function formatUserInfo(userInfo) {
    var Username = userInfo.Username;
    var UserCreateDate = userInfo.UserCreateDate;
    var UserStatus = userInfo.UserStatus;
    var enabled = userInfo.Enabled;
    var enable_btn;
    var email_verified;
    var name;

    if (UserStatus === 'CONFIRMED') {
        UserStatus = `<span class="badge badge-success">Active</span>`
    } else {
        UserStatus = `<span class="badge badge-purple">Pending Activation</span>`
    }

    if (enabled) {
        enabled = `<i class="mdi mdi-account-check"></i><span class="text-success">Enabled</span>`;
        enable_btn = `<button class="btn btn-xs btn-icon waves-effect waves-light btn-danger" title="Disable User" id="disable-user-btn"><i class="mdi mdi-account-off"></i> </button>`
    } else {
        enabled = `<i class="mdi mdi-account-off"></i><span class="text-danger">Disabled</span>`;
        enable_btn = `<button class="btn btn-xs btn-icon waves-effect waves-light btn-success" title="Enable User" id="enable-user-btn"><i class="mdi mdi-account-check"></i> </button>`
    }

    var userAttributesArr = userInfo.UserAttributes;
    for (var i = 0; i < userAttributesArr.length; i++) {

        if (userAttributesArr[i].Name === "email_verified") {
            email_verified = (userAttributesArr[i].Value === "true") ? true : false;
            if (email_verified) {
                email_verified = `<i class="mdi mdi-shield-check"></i><span class="text-success">Verified</span>`
            } else {
                email_verified = `<i class="mdi mdi-shield-lock"></i><span class="text-warning">Pending</span>`
            }
        } else {
            email_verified = `<i class="mdi mdi-shield-check"></i><span class="text-success">Verified</span>`
        }

        if (userAttributesArr[i].Name === "name") {
            name = userAttributesArr[i].Value;
        }

    }

 
    var options = ``
    options = `<div class="row">
    
    <div class="col-md-6"> <button class="btn btn-xs btn-dark dropdown-toggle waves-effect waves-light" data-userid="${Username}" data-toggle="dropdown" aria-expanded="false"> <i class="mdi mdi-account-plus mr-1"></i> <span>Add User To Group</span> </button>
    <div class="dropdown-menu" data-userid="${Username}" id="btn-addUsertogroup">
                                                      
    <a href="javascript:void(0);"  class="dropdown-item groupname_id" data-groupname="Admin">Admin</a>
                                                       
    <a href="javascript:void(0);"  class="dropdown-item groupname_id" data-groupname="Monitoring">Monitoring</a>
                                                       
    <a href="javascript:void(0);"  class="dropdown-item groupname_id" data-groupname="Opretional">Opretional</a>
    </div>  </div>

    <div class="col-md-6 text-right action-btns">${enable_btn}</div>
    </div><hr>
    <div class="row" id="user-info-container">
    <div class="col-md-12"><h4 class="info-username" id="info_username" data-username="${Username}">${name}</h4></div>
    <div class="col-md-4"><h5 class="info-role text-muted">Role : </h5></div><div class="col-md-8" style="padding-top: 8px;">User</div>
    <div class="col-md-4"><h5 class="info-joined text-muted">Joined : </h5></div><div class="col-md-8" style="padding-top: 8px;">${moment(UserCreateDate).format('lll')}</div>
    <div class="col-md-4"><h5 class="info-email text-muted">Email : </h5></div><div class="col-md-8" style="padding-top: 8px;">${email_verified}</div>
    <div class="col-md-4"><h5 class="info-enabled text-muted">Enabled : </h5></div><div class="col-md-8" style="padding-top: 8px;">${enabled}</div>
    <div class="col-md-6"> </div>
    </div>`

$('.user-overview-container').html(options)
}




function getListOfGroups(){
    
    $.get('/v1/users/getListOfGroups',
    function (data, status) {
        if (data.status == 200) {
            $('#addUsertogroup').empty()
           var _groups = data.data.Groups;
           console.log("getListOfGroups >> : " + JSON.stringify(_groups))
           var addgroups = ``
           for (var i=0; i<_groups.length; i++){
             var group_Name = _groups[i].GroupName;
             console.log("group_Name >> : " + JSON.stringify(group_Name))
           addgroups += `<a href="javascript:void(0);"  class="dropdown-item groupname_id" data-groupname="${group_Name}">${group_Name}</a> `
           }
            $('#addUsertogroup').html(addgroups)
        }
    });
}
getListOfGroups()


// function hidecheckbox() {
//     $('#groupaddname').hide()
//     $('.hidegroup').hide()
// }
// hidecheckbox()
























// getUserInfo
function getUserInfo(username) {
    $.get(`/v1/users/getUserInfo/${username}`,
        function (data, status) {
            if (data.status == 200) {
                console.log("getUserInfo >> : " + JSON.stringify(data))
                formatUserInfo(data.data)
            }
        });
}


// User row selection
$(document).on("click", ".users-tbl-row", function () {

    $('.users-tbl-row').removeClass("active");
    $(this).addClass("active");

    var username = $(this).attr('id');
    getUserInfo(username);

})




// getGroupInfo
function getGroupInfo(groupname) {
    $.get(`/v1/users/getGroupInfo/${groupname}`,
        function (data, status) {
            if (data.status == 200) {
                formatGroupInfo(data.data.Group)
            }
        });
}



// Group row selection
$(document).on("click", ".all-groups-tbl-row", function () {

    $('.all-groups-tbl-row').removeClass("active");
    $(this).addClass("active");
    var groupname = $(this).attr('id')

    getGroupInfo(groupname);

})


// Enable User
$(document).on("click", "#enable-user-btn", function () {

    let params = {
        username: $('#info_username').attr('data-username')
    }

    $.post(`/v1/users/enableUser`,
        params,
        function (data, status) {
            if (data.status === 200) {

                toastr.success('User Enabled');

                setTimeout(function () {
                    window.location.href = '/v1/users';
                }, 3000)

            } else {
                toastr.error('User Disable Failed');
            }
        });

})

// Disable User
$(document).on("click", "#disable-user-btn", function () {

    let params = {
        username: $('#info_username').attr('data-username')
    }

    $.post(`/v1/users/disableUser`,
        params,
        function (data, status) {
            if (data.status === 200) {

                toastr.success('User Disabled');

                setTimeout(function () {
                    window.location.href = '/v1/users';
                }, 3000)

            } else {
                toastr.error('User Disable Failed');
            }
        });

})



// // add User To Group
$(document).on("click", ".groupname_id", function () {

    let params = {
        username: $(this).parent().attr('data-userid'),
        groupname: $(this).attr('data-groupname'),

    }

    // $.post(`/v1/users/addUserToGroup`,
    //     params,
    //     function (data, status) {
    //         if (data.status === 200) {

    //             toastr.success('User Added To Group');

    //             setTimeout(function () {
    //                 window.location.href = '/v1/users';
    //             }, 3000)

    //         } else {
    //             toastr.error('User Not Added To Group');
    //         }
    //     });

})




// Create Group
$("#createGroup-form").submit(function (event) {

    $.post('/v1/users/createGroup',
        {
            groupname: $('#groupName').val().trim(),

        },

        function (data, status) {
            if (data.status === 200) {
                groups = data.data.groups;

                toastr.success('Group Create successfully.');

               $('#createGroupModal').modal('hide');

                setTimeout(function () {
                    window.location.href = '/v1/users'

                }, 3000)

            } else {
                toastr.danger('Group Creation failed.')
            }

        });
     event.preventDefault()
})



// code for show hide button of userCreate & groupCreate 

$(document).ready(function () {

    $('#groupbtn').hide(0);  //or do it through css
    $('#usertab').click(function () {
        $('#userbtn').show();
        $('#groupbtn').hide(0);

    });

    //otherTab is the class for the tabs other than tab1
    $('#grouptab').click(function () {
        $('#userbtn').hide(0);
        $('#groupbtn').show(0);

    });


    var tab_type = window.location.hash.substr(1);

    if (tab_type === 'groups-tab') {
        $('#usertab').removeClass('active');
        $('#users-tab').removeClass('active');
        $('#users-tab').removeClass('show');
        $('#grouptab').addClass('active');
        $('#groups-tab').addClass('active');
        $('#groups-tab').addClass('show');
    }

});


// Delete Group
$(document).on("click", ".delete-group", function () {

    var params = {
        groupname: $(this).closest(".action-td").attr('id'),
    }

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if (t.value) {
            $.post('/v1/users/deleteGroup',
                params,
                function (data, status) {
                    if (data.status === 200) {

                        toastr.success('Group Deleted');

                        setTimeout(function () {
                            window.location.href = '/v1/users';
                        }, 3000)

                    } else if (data.status === 401) {
                        toastr.error('Delete Group Failed');
                    }
                });
        }
    })
})



