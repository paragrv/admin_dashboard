function openSettings(evt, Name) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(Name).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();


$(document).ready(function () {
    // change password
    $("#changePasswordForm").submit(function (event) {

        var params = {
            currentpassword: $("#currentPassword").val().trim(),
            newpassword: $("#newPassword").val().trim()
        }
        console.log(" change password > " +JSON.stringify(params))
        $.post(`/v1/users/changePassword`,
            params,
            function (data, status) {
                if (data.status === 200) {
                    toastr.success('Password change successfully');
                    setTimeout(function () {
                        window.location.href = '/';
                    }, 3000)
                } else {
                    toastr.error(data.message);
                }
            });
        event.preventDefault()
    })

    // update user attributes
    $('#update_name_form').submit(function (event) {

        var params = {
            UserAttributes: [{
                Name: 'name',
                Value: $("#name").val().trim(),
            }]

        }
        updateUserAttributes(params);
        event.preventDefault()
    })

    //update username
    $('#update_username_form').submit(function (event) {

        var params = {
            UserAttributes: [{
                Name: 'name',
                Value: $("#username").val().trim(),
            }]

        }
        updateUserAttributes(params);
        event.preventDefault()

    })
})
function updateUserAttributes(params) {

    $.post(`/v1/users/updateUserAttributes`,
        params,
        function (data, status) {
            if (data.status === 200) {
                toastr.success('Update Name successfully');
                setTimeout(function () {
                    window.location.href = '/v1/users/settings';
                }, 3000)
            } else {
                toastr.error(data.message);
            }
        });

    
}

