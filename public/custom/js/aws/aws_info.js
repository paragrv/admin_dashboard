

$("#awsInfo-form").submit(function (event){
    var  accessKey=  $("#accessKeyId").val().trim();
    var secretAccessKey= $("#secretAccessKey").val().trim();
    var region=  $("#region").val().trim()
    var params={
        region:region,
        secretAccessKey:secretAccessKey,
        accessKey:accessKey
       }
       alert(JSON.stringify(params))

         $.post('/v1/aws_info/awsinfo_stored_inMongodb',
    params,
    function (data, status) {
        if (data.status === 200) {
            toastr.success('Aws Information Stored Successfully');
            setTimeout(function () {
                window.location.reload();
            }, 2000);
        } else {
            toastr.error(data.message);
        }
    });
    event.preventDefault()
})


function getAwsInfo(){
    
    return new Promise((resolve, reject) => {
    $.get('/v1/aws_info/getAwsInfo',
    function (data, status) {
        if (data.status === 200) {
            destroyRows()
            var awsinfodata = data.data;
            console.log(JSON.stringify(awsinfodata))
            resolve(awsinfodata)
           awsInfoData(awsinfodata)
          
        } else {
            toastr.error(data.message);
        }
    });
})
}
getAwsInfo()

function destroyRows() {
    $('#aws_Info_tbody').empty()
    $('#aws_Info_table').DataTable().rows().remove();
    $("#aws_Info_table").DataTable().destroy()
}

function  awsInfoData(awsinfodata){
    var array =awsinfodata;
    var options_table = "";

    array.forEach(function (element, i) {
      var region =  element.region ? element.region: "";
      var secretAccessKey =  element.secretAccessKey ? element.secretAccessKey: "";
      var accessKey = element.accessKey ? element.accessKey: "";
      console.log(region)
      console.log(secretAccessKey)
      console.log(accessKey)
      options_table += `<tr class="aws-tbl-row asset-row">
          <td class="accessKey">${accessKey}</td>
          <td class="secretAccessKey">${secretAccessKey}</td>
          <td class="region">${region}</td>`;
          
          if (i == array.length - 1) {

            //initiate for 1st row
           
            $('#aws_Info_tbody').append(options_table)
            reInitializeDataTable()
        }
    })

}
var aws_Info_table
function reInitializeDataTable() {
    $("#aws_Info_table").DataTable().destroy()
    aws_Info_table = $('#aws_Info_table').DataTable({
    
    })


}

$(document).ready(function () {

    getAwsInfo().then((getAwsInfo) =>{
        if (getAwsInfo.length >= 1){
            $('#awsbtn').hide();  
        }else{
            $('#awsbtn').show();
        }
    })
})