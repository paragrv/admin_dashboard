var express = require('express');
var router = express.Router();

const tenant = require("../workers/tenant");
const dashboardTenantObj = new tenant.TenantClass();


const awsInfo = require("../workers/awsInfo");
const awsInfoObj = new awsInfo.AwsInfoClass();

// ##############################################################################

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');

// ########## MICRO-3 ##########
let mcube_cognito_BASE = API.mcube_cognito.base.development;

const cognito = require('../app/utilities/user_management/cognito');


/* GET users listing. */
router.get('/', function (req, res, next) {
   const tenantId = req.session.tenantId ? req.session.tenantId : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('aws_info/index', { section: 'Admin Portal', sub_section: '', userData: userData, userName: userName, userRole: userRole, tenantId: tenantId });
});


//
router.post('/awsinfo_stored_inMongodb', (req, res, next) => {

  console.log("awsinfo_stored_inMongodb POST called")
var  tenantName = req.session.tenantId ? req.session.tenantId : null
var tenantName_awsInfo = `${tenantName}_awsInfo`

  const params = {
    accessKey: req.body.accessKey ? req.body.accessKey : null,
    secretAccessKey: req.body.secretAccessKey ? req.body.secretAccessKey : null,
    region: req.body.region ? req.body.region : null,
    tenantName_awsInfo:tenantName_awsInfo
  };
console.log("awsinfo_stored_inMongodb after called >> "+JSON.stringify(params))
  awsInfoObj.save(params).then((response) => {
    if (response) {
      console.log('createGroup response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Group Create Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN GROUP CREATION: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});

// Get All Users
router.get('/getAwsInfo', (req, res, next) => {
    console.log('***GET ALL getAwsInfo CALLED***');
    var  tenantName = req.session.tenantId ? req.session.tenantId : null
    var tenantName_awsInfo = `${tenantName}_awsInfo`
    const params = {
        tenantName_awsInfo:tenantName_awsInfo
    }
    awsInfoObj.findBytenantName_awsInfo(params).then((data) => {
        res.json({
            status: 200,
            message: 'AwsInfo by findBytenantName_awsInfo fetched successfully',
            data: data,
        });
    })
  });

module.exports = router;