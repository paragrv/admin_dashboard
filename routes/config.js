const express = require('express');
const router = express.Router();

const AWS = require('aws-sdk');
const tenantConfig = require('../config/tenantConfig');
var fs = require("fs");


router.post('/addTenantConfig', (req, res, next) => {
    console.log(" addTenantConfig POST called in UI")

    var config = req.body.config;
    var tenantName = req.body.config.poolName;
    var tenantId = req.body.config.poolId;

    tenantConfig[tenantId] = { tenantName: tenantName, config };

    fs.writeFile('config/tenantConfig.json', JSON.stringify(tenantConfig, null, 2), (err) => {
        if (err) {
            console.error(err);
            return;
        } else {
            res.json({
                status: 200,
                message: `Tenant config udated in mcubeUI`,
                data: null
            });
        }

    });

})

router.post('/deleteTenantConfig', (req, res, next) => {
    console.log("deleteTenantConfig called in API");

    var params = {
        userPoolId: req.body.userPoolId ? req.body.userPoolId : null,
        poolName: req.body.poolName ? req.body.poolName : null

    }

    delete tenantConfig[params.userPoolId];
    fs.writeFile('config/tenantConfig.json', JSON.stringify(tenantConfig, null, 2), (err) => {
        if (err) {
            console.error(err);
            return;
        } else {
            res.json({
                status: 200,
                message: `Tenant config deleted in mcubeUI`,
                data: null
            });
        }

    });
})
module.exports = router;
