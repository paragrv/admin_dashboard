var express = require('express');
var router = express.Router();

const tenant = require("../workers/tenant");
const dashboardTenantObj = new tenant.TenantClass();

// ##############################################################################

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');

// ########## MICRO-3 ##########
let mcube_cognito_BASE = API.mcube_cognito.base.development;

const cognito = require('../app/utilities/user_management/cognito');


/* GET users listing. */
router.get('/', function (req, res, next) {
   const tenantId = req.session.tenantId ? req.session.tenantId : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/index', { section: 'Admin Portal', sub_section: '', userData: userData, userName: userName, userRole: userRole, tenantId: tenantId });
});


router.get('/user-groups', function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  // var params=userData.accessToken.jwtToken;
  // console.log("params data > "+JSON.stringify(params))
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/user-groups', { section: 'Users Listing', sub_section: '', userData: userData, userName: userName, userRole: userRole });
})


// registration
router.post('/register', (req, res, next) => {

  console.log("register POST called")

  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  const tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId,
    name: req.body.name ? req.body.name : null,
    email: req.body.email ? req.body.email : null,
    password: req.body.password ? req.body.password : null
  }


  cognito.RegisterUser(tenantParams).then((response) => {
    if (response) {
      console.log('user data saved response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Saved Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER SAVE: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
  })
});


// group creation
// router.post('/createGroup', (req, res, next) => {

//   console.log("createGroup POST called")

//   const params = {
//     groupname: req.body.groupname ? req.body.groupname : null,

//   };

//   cognito.createGroup(params).then((response) => {
//     if (response) {
//       console.log('createGroup response >> ' + JSON.stringify(response));
//       res.json({
//         status: 200,
//         message: 'Group Create Successfuly',
//         data: response,
//       })
//     }
//   }).catch((err) => {
//     console.log(`error IN GROUP CREATION: ${err}`);
//     res.json({
//       status: 500,
//       message: 'Oops ! Some error occured, please try again later.',
//       data: null,
//     });
//   });

// });



// Get All Users
router.get('/getUsers', (req, res, next) => {
  console.log('***GET ALL getUsers CALLED***');


  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  let tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId
  }

  cognito.getUsers(tenantParams).then((users) => {
    res.json({
      status: 200,
      message: 'Users fetched successfully',
      data: users,
    });
  });
  })
});


// Delete User
router.post('/deleteUser', (req, res, next) => {
  console.log("deleteUser POST called")
  const params = {
    username: req.body.username ? req.body.username : null
  };

  cognito.deleteUser(params).then((response) => {
    if (response) {
      console.log('user delete response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Deleted Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Delete: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// getUserInfo
router.get('/getUserInfo/:username', (req, res, next) => {
  console.log('***getUserInfo CALLED***');
  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  let tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId,
    username: req.params.username ? req.params.username : null
  }

  cognito.getUserInfo(tenantParams).then((user) => {
    res.json({
      status: 200,
      message: 'getUserInfo fetched successfully',
      data: user,
    });
  });
  })
})


// disableUser
router.post('/disableUser', (req, res, next) => {
  const params = {
    username: req.body.username ? req.body.username : null
 };
 cognito.disableUser(params).then((response) => {
    if (response) {
      console.log('disableUser response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Disabled Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Disable: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})


// enableUser
router.post('/enableUser', (req, res, next) => {
  console.log("enableUser POST called")
  const params = {
    username: req.body.username ? req.body.username : null
  };

  cognito.enableUser(params).then((response) => {
    if (response) {
      console.log('enableUser response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Enabled Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Enable: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// Change Password
router.post('/changePassword', (req, res, next) => {
  console.log("ChangePassword POST called")
  const params = {
    accesstoken: req.session.userData ? req.session.userData.accessToken.jwtToken : null,
    currentpassword: req.body.currentpassword ? req.body.currentpassword : null,
    newpassword: req.body.newpassword ? req.body.newpassword : null
  }
  cognito.changePassword(params).then((response) => {
    if (response) {
      setTimeout(function () {
        req.session.destroy(function (err) {
          if (err) {
            res.redirect('/login');
          } else {
            req.session = null;
          }
        });
      }, 3000)
      res.json({
        status: response.status,
        message: response.message,
        data: response.data,
      })
    }
  }).catch((err) => {
    console.log(`error in Password: ${err}`);
    res.json({
      status: err.status,
      message: err.message,
      data: err.data,
    })
  });
})


// Update User Attributes
router.post('/updateUserAttributes', (req, res, next) => {
  console.log("Update User Attribute  POST called")
  const params = {
    accesstoken: req.session.userData ? req.session.userData.accessToken.jwtToken : null,
    UserAttributes: req.body.UserAttributes ? req.body.UserAttributes : null,
  }
  console.log("update user attributes > " + JSON.stringify(params))
  cognito.updateUserAttributes(params).then((response) => {
    if (response) {
      console.log('update user response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Update Successfully',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

})


// add User To Group
router.post('/addUserToGroup', (req, res, next) => {
  console.log("addUserToGroup POST called")
  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  const tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId,
    username: req.body.username ? req.body.username : null,
    groupname: req.body.groupname ? req.body.groupname : null
  }
  console.log(JSON.stringify(tenantParams))

  cognito.addUserToGroup(tenantParams).then((response) => {
    if (response) {
      console.log('addUserToGroup response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Added To Group Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN addUserToGroup: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})
})





// Create Group
router.post('/createGroup', (req, res, next) => {
  console.log("createGroup POST called")
  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  const tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId,
    groupname: req.body.groupname ? req.body.groupname : null
  }



  cognito.createGroup(tenantParams).then((response) => {
    if (response) {
      console.log('createGroup response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Group Created Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN Group Creation: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})
})

// Get All Groups
router.get('/getListOfGroups', (req, res, next) => {
  console.log('***GET getListOfGroups CALLED***');
  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  const tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId
  }
  console.log(JSON.stringify(tenantParams))

  cognito.getListOfGroups(tenantParams).then((groups) => {
    res.json({
      status: 200,
      message: 'List Of Groups fetched successfully',
      data: groups,
    });
  });
  })
});



// Delete User
router.post('/deleteGroup', (req, res, next) => {
  console.log("deleteGroup POST called")
  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  const tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId,
    groupname: req.body.groupname ? req.body.groupname : null
  }
  console.log(JSON.stringify(tenantParams))

  cognito.deleteGroup(tenantParams).then((response) => {
    if (response) {
      console.log('group delete response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Group Deleted Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN Group Delete: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})
})



// getGroupInfo
router.get('/getGroupInfo/:groupname', (req, res, next) => {
  console.log('***getGroupInfo CALLED***');
  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  const tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId,
    groupname: req.params.groupname ? req.params.groupname : null
  }
  console.log(JSON.stringify(tenantParams))

  cognito.getGroupInfo(tenantParams).then((group) => {
    res.json({
      status: 200,
      message: 'getGroupInfo fetched successfully',
      data: group,
    });
  });
  })
})



// Get List Users In Group
router.get('/listUsersInGroup/:groupname', (req, res, next) =>{

  console.log('***GET ListUsersInGroup CALLED***');
  const params = {
    tenantName : req.session.tenantId ? req.session.tenantId : null
  }
  dashboardTenantObj.findBytenantId(params).then((data) => {
    var array =data;

    for (var i=0; i<array.length; i++){
      var ClientId = array[i].clientId;
      var UserPoolId = array[i].tenantId;  
    }
  console.log(JSON.stringify(ClientId))
  console.log(JSON.stringify(UserPoolId))

  const tenantParams ={
    UserPoolId:UserPoolId,
    ClientId: ClientId,
    groupname: req.params.groupname ? req.params.groupname : null
  }
  console.log(JSON.stringify(tenantParams))


  cognito.listUsersInGroup(tenantParams).then((users) => {
    res.json({
      status: 200,
      message:'List Of Users in Group fetched successfully',
      data: users,
    });
  });
})
});


router.get('/settings', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;

  res.render('user/settings', { section: 'Admin Portal', sub_section: 'Settings', userData: userData, userName: userName, userRole: userRole });

});





// Delete User
router.post('/adminRemoveUserFromGroup', (req, res, next) => {
  console.log("adminRemoveUserFromGroup POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.adminRemoveUserFromGroup,
    data: {
      groupname: req.body.groupname ? req.body.groupname : null,
      username: req.body.username ? req.body.username : null
    },
    headers: {
      token : req.session.userData.idToken.jwtToken
    }
  };

  //console.log("routes admin remove user group > " + JSON.stringify(params))
  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN User Removed From Group: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

module.exports = router;