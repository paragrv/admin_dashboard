var express = require('express');
var router = express.Router();



const tenant = require("../workers/tenant");
const dashboardTenantObj = new tenant.TenantClass();


const cognito = require('../app/utilities/user_management/cognito');


router.get('/user-Authentication', function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  // var params=userData.accessToken.jwtToken;
  // console.log("params data > "+JSON.stringify(params))
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/authenticateUser', { section: 'Users AuthenticateUser', sub_section: '', userData: userData, userName: userName, userRole: userRole });
})

router.get('/', function (req, res, next) {
 
 let user_id = req.session ? (req.session.user_id ? req.session.user_id : null) : null;
 

 console.log("req.session>.user_id >> " + JSON.stringify(user_id))
 if (user_id) {

   console.log("I SHOULD REDIREC!!")
   
   res.redirect('/v1/users');
  
 } else {
   console.log("I SHOULD LOGIN!")
   res.render('login');
 }
});


// Login
router.post('/login', (req, res, next) => {
  console.log("LOGIN POST called" + JSON.stringify(req.body))
  let params = {
    tenantName: req.body.tenantId,
    username: req.body.username,
    password: req.body.password,
  }
console.log("login"+JSON.stringify(params))

dashboardTenantObj.findBytenantId(params).then((data) => {
  console.log("datttt >> "+JSON.stringify(data))
  var array =data;
  console.log("array >> "+JSON.stringify(array))
  for (var i=0; i<array.length; i++){
    console.log(" i m in for loop")
    var ClientId = array[i].clientId;
    var UserPoolId = array[i].tenantId;
   // var tenantName = array[i].tenantName;

  }
console.log(JSON.stringify(ClientId))
console.log(JSON.stringify(UserPoolId))
let tenantParams ={
  UserPoolId:UserPoolId,
  ClientId: ClientId,
  tenantId: req.body.tenantId,
  username: req.body.username,
  password: req.body.password,
}
console.log(tenantParams)

  cognito.Login(tenantParams).then((response) => {
    if (response.status === 200) {
      //console.log('user sign in response >> ' + JSON.stringify(response));
      req.session.user_id = "ADMIN";

      //set user details in sesssion
      req.session.userData = response.data;
      req.session.userName = response.data.idToken.payload.email;
      req.session.userRole = (response.data.idToken.payload['cognito:groups']) ? (response.data.idToken.payload['cognito:groups']) : 'User';
      req.session.tenantId = tenantParams.tenantId
      
      console.log("tenantId>>>>>>>>>>>>>>>>>>>>  " + req.session.tenantId)
      console.log("USER Info >>>>>   ")
      console.log("userData>>>>>>>>>>>>>>>>>>>>   " + JSON.stringify(req.session.userData))
      console.log("userName>>>>>>>>>>>>>>>>>>>>   " + req.session.userName)
      console.log("userRole>>>>>>>>>>>>>>>>>>>>  " + req.session.userRole)


      res.json({
        status: 200,
        message: 'Login successful'
      });
    } else {
      res.json({
        status: 403,
        message: response.message,
        data: response.data,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER sign in : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

});


//Logout
router.get('/logout', function (req, res) {
  req.session.destroy(function (err) {
    if (err) {
      res.redirect('/');
    } else {
      req.session = null;
      console.log("Logout Success " + JSON.stringify(req.session) + " ");
      res.redirect('/');
    }
  });
});

router.get('/404', function (req, res, next) {
  console.log('BEFORE')

  setTimeout(() => {
    console.log('END OF SET TIMEOUT')
    res.render('404', { title: '404 Error' });
  }, 120000);
});

router.get('/temp', function (req, res, next) {
  res.render('temp');
});




// Login
router.post('/authenticateUser', (req, res, next) => {
  console.log("AuthenticateUser")

  var params  = {
   tenantName: req.body.tenantName,
   username: req.body.username,
   password: req.body.password,
   newPassword: req.body.newPassword 
}
console.log("params ////// ??? " +JSON.stringify(params))
dashboardTenantObj.findBytenantId(params).then((data) => {
  console.log("datttt >> "+JSON.stringify(data))
  var array =data;
  console.log("array >> "+JSON.stringify(array))
  for (var i=0; i<array.length; i++){
    console.log(" i m in for loop")
    var ClientId = array[i].clientId;
    var UserPoolId = array[i].tenantId;

  }
console.log(JSON.stringify(ClientId))
console.log(JSON.stringify(UserPoolId))
let tenantParams ={
  username: req.body.username,
  password: req.body.password,
  newPassword: req.body.newPassword,
  UserPoolId:UserPoolId,
  ClientId:ClientId
}


  cognito.AuthenticateUser(tenantParams).then((response) => {
    if (response.status === 200) {
      console.log('user sign in response >> ' + JSON.stringify(response));
    res.json({
        status: 200,
        message: 'Login successful'
      });
    } else {
      res.json({
        status: 403,
        message: response.message,
        data: response.data,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER sign in : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
});


});





////////////////////////////////////////////////////////////////////////////////////////


module.exports = router;